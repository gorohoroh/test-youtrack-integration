# Test YouTrack integration

## Docs to read and follow:
* [x] [GitLab Integration](https://www.jetbrains.com/help/youtrack/standalone/Integration-with-GitLab.html)
* [x] [Integrate with GitLab](https://www.jetbrains.com/help/youtrack/standalone/integrate-project-with-gitlab.html)
* [x] [Link Issues in VCS Commits](https://www.jetbrains.com/help/youtrack/standalone/Map-Issues-to-VCS-Change-Commit.html)
* [x] [Commits](https://www.jetbrains.com/help/youtrack/standalone/VCS-Changes.html#view-edit-vcs-commits)
* [x] [Pull Requests](https://www.jetbrains.com/help/youtrack/standalone/VCS-Changes.html#pull-requests)
* [x] [Apply Commands in VCS Commits](https://www.jetbrains.com/help/youtrack/standalone/Apply-Commands-in-VCS-Commits.html)

## Scenarios to test
* [x] Add links to YouTrack issues in commit messages
* [x] A commit that referenced an issue is displayed in the activity stream of the referenced issue
* [x] Mention a YouTrack user from commit
* [x] A merge request that mentions an issue in title or summary is reflected in this issue's activity stream (but merging the MR doesn't automatically close the issue unless it's mentioned)
* [x] ~~A merge request that mentions an issue resolution command in title or summary closes the issue when it's merged~~ (*this won't work because YouTrack ignores command syntax in MR titles and descriptions*)
* [x] When a merge request contains a commit that includes an issue fix command or another command, YouTrack applies the command when the MR is merged.


## What YouTrack's GitLab integration can do

YouTrack's GitLab integration helps you:
* Add links to YouTrack issues in commit messages.
* Apply commands to YouTrack issues (such as close issues, change their types, leave comments, etc.) from commit messages.
* Track commits that are related to specific issues in the activity stream for each issue.
* Display the status of merge requests directly in the activity stream of any issue that is referenced in the title or description of the pull request.

Here's what GitLab integration settings look like in YouTrack's admin UI:

![YouTrack's GitLab integration settings, 1](img/youtrack_admin_gitlab_integration_settings_1.png)

![YouTrack's GitLab integration settings, 2](img/youtrack_admin_gitlab_integration_settings_2.png)

Adding a YouTrack issue ID to a commit message helps transform the issue ID to a YouTrack link:

![Commit message with a YouTrack issue ID in GitLab's commit details UI](img/link_to_issue_commit_details.png)

![Commit message with a YouTrack issue ID in GitLab's commit history UI](img/link_to_issue_commit_history.png)

A link to YouTrack is also generated when you add an issue ID to a Markdown document:

![Commit message with a YouTrack issue ID in GitLab's commit history UI](img/link_to_issue_commit_readme.png)

When you reference a YouTrack issue ID in a commit message, you can also expect to see the commit mentioned in the issue's activity stream:

![Commit mentioned in YouTrack issue's activity stream](img/issue_activity_stream_commit_that_references_issue.png)

If someone has opened a GitLab merge request (MR) where the title or description mentions a YouTrack issue ID, that MR will also be visible in the issue's activity stream:

![Merge request that mentions a YouTrack issue ID in GitLab UI](img/merge_request_gitlab.png)

![Merge request that mentions a YouTrack issue ID in YouTrack's issue activity stream](img/merge_request_youtrack.png)

You can include YouTrack commands into commit messages and have them applied to the issues that you reference. For example, here's how you can change the state of issue GLIT-2 to "In Progress":

![Setting a YouTrack issue to In Progress with a commit message](img/command_from_commit_message_gitlab.png)

As soon as that commit is processed, you can see in YouTrack's UI that GLIT-2 has been set to "In Progress":

![YouTrack issue set to In Progress with a commit message](img/command_from_commit_message_youtrack.png)

In your commit messages, you can apply multiple commands to one or more issues. For example, the command `(#GLIT-2, #GLIT-1) for me type task` assigns two issues, GLIT-1 and GLIT-2, to you and sets them to type "Task":

![YouTrack commands in a commit message](img/commands_from_commit_gitlab.png)

If you go to YouTrack's UI, you can see that the commands have been applied:

![A comment applied to a YouTrack issue from a commit message](img/commands_from_commit_youtrack.png)

In addition to other kinds of commands, you can add issue comments and mention YouTrack users, all from commit messages. The syntax to use is as follows: `#GLIT-1 @youtrack_username Comment text`.

![YouTrack comment from a commit message, as seen in GitLab UI](img/comment_in_commit_message.png)

When you go to YouTrack UI, you can see that the comment has, in fact, been published, and the username syntax has been resolved to mention a specific user: 

![YouTrack comment published from a commit message](img/comment_from_commit_message_to_youtrack_comment.png)

Configuring YouTrack's GitLab integration to only track commands in commits to certain branches (such as `develop` and `master`) while ignoring commits from all other branches lets you close issues exactly when your merge requests are merged.

For example, let's say your merge request contains a commit with a command to fix an issue, such as `#GLIT-2 fixed` in the following example:

![Fix command in merge request](img/fix_and_comment_from_commit_message_when_mr_merged_gitlab.png)

When this commit is made to a MR that is in progress, the commands that it contains are not applied. However, when the MR is merged into a branch that YouTrack's GitLab integration tracks, then the YouTrack commands in the MR's commits are processed and applied:

![Fix command applied to a YouTrack issue exactly on merging the MR](img/fix_and_comment_from_commit_message_when_mr_merged_youtrack.png)